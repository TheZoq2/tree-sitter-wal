===
empty defun works
===

(defun name [] )

---

(source
  (defun
    (sym_lit (sym_name))))


===
defun with one param works
===

(defun name [a] )

---

(source
  (defun
    (sym_lit (sym_name))
    (parameter (sym_lit (sym_name)))))



===
defun with multiple params works
===

(defun name [a b] )

---

(source
  (defun
    (sym_lit (sym_name))
    (parameter (sym_lit (sym_name)))
    (parameter (sym_lit (sym_name)))))


===
defun with multiple params and bodyworks
===

(defun name [a b] c)

---

(source
  (defun
    (sym_lit (sym_name))
    (parameter (sym_lit (sym_name)))
    (parameter (sym_lit (sym_name)))
    (sym_lit (sym_name))))




===
empty defmacro works
===

(defmacro name [] )

---

(source
  (defmacro
    (sym_lit (sym_name))))


===
defmacro with one param works
===

(defmacro name [a] )

---

(source
  (defmacro
    (sym_lit (sym_name))
    (parameter (sym_lit (sym_name)))))



===
defmacro with multiple params works
===

(defmacro name [a b] )

---

(source
  (defmacro
    (sym_lit (sym_name))
    (parameter (sym_lit (sym_name)))
    (parameter (sym_lit (sym_name)))))


===
defmacro with multiple params and bodyworks
===

(defmacro name [a b] c)

---

(source
  (defmacro
    (sym_lit (sym_name))
    (parameter (sym_lit (sym_name)))
    (parameter (sym_lit (sym_name)))
    (sym_lit (sym_name))))
